package com.company.mes.sports.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.Sport;
import com.company.mes.sports.models.Student;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

import static com.company.mes.sports.activities.MainActivity.standardsList;
import static com.company.mes.sports.activities.MainActivity.standards_adapter;

public class AddStudent extends AppCompatActivity {

    EditText txtId, txtName, txtDob, txtGuardian, txtAddress, txtPhone, txtBlood;
    RadioButton rdoMale;
    Spinner spr_interested_singe_item, spr_interested_group_item, sprStandard;

    String TAG = "AddStudent";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        getSupportActionBar().setTitle("Add Student");

        rdoMale = (RadioButton)findViewById(R.id.male);
        txtId = (EditText) findViewById(R.id.admno);
        txtName = (EditText) findViewById(R.id.name);
        txtDob = (EditText) findViewById(R.id.dob);
        txtGuardian = (EditText) findViewById(R.id.guardian);
        txtAddress = (EditText) findViewById(R.id.address);
        txtPhone = (EditText) findViewById(R.id.phone);
        txtBlood = (EditText) findViewById(R.id.blood);
        spr_interested_singe_item = (Spinner)findViewById(R.id.spr_interested_singe_item);
        spr_interested_group_item = (Spinner)findViewById(R.id.spr_interested_group_item);
        sprStandard = (Spinner)findViewById(R.id.spr_standard);

        ArrayAdapter<String> single_items_adapter = new ArrayAdapter<>(AddStudent.this,
                android.R.layout.simple_spinner_dropdown_item,
                MainActivity.sportsNameList(MainActivity.sportsList(Sport.SINGLE)));
        spr_interested_singe_item.setAdapter(single_items_adapter);

        ArrayAdapter<String> group_items_adapter = new ArrayAdapter<>(AddStudent.this,
                android.R.layout.simple_spinner_dropdown_item,
                MainActivity.sportsNameList(MainActivity.sportsList(Sport.GROUP)));
        spr_interested_group_item.setAdapter(group_items_adapter);

        sprStandard.setAdapter(standards_adapter);

    }

    public void createStudent(View view) {
        if (isValid()){
            Student student = new Student();
            student.setId(txtId.getText().toString())
                    .setName(txtName.getText().toString())
                    .setGender(rdoMale.isChecked() ? "Male" : "Female")
                    .setDob(txtDob.getText().toString())
                    .setStandard(standardsList.get(sprStandard.getSelectedItemPosition()))
                    .setGuardian(txtGuardian.getText().toString())
                    .setAddress(txtAddress.getText().toString())
                    .setPhone(txtPhone.getText().toString())
                    .setBlood(txtBlood.getText().toString())
                    .setInterested_singe_item(MainActivity.sportsList(Sport.SINGLE).get(spr_interested_singe_item.getDropDownVerticalOffset()))
                    .setInterested_group_item(MainActivity.sportsList(Sport.GROUP).get(spr_interested_singe_item.getDropDownVerticalOffset()));

            Log.d(TAG, "createStudent: "+student.toString());
            MainActivity.databaseReference.child("students").child(txtId.getText().toString())
                    .setValue(student)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(AddStudent.this,R.string.successfully_created, Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "onFailure: createStudent", e);
                    Toast.makeText(AddStudent.this,R.string.oops, Toast.LENGTH_LONG).show();
                }
            });

        }

    }

    private boolean isValid() {
        return true;
    }
}
