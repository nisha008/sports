package com.company.mes.sports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.mes.sports.R;
import com.company.mes.sports.models.AttendanceStatus;

import java.util.List;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {
    private List<AttendanceStatus> attendanceStatusList;
    private Context context;

    public AttendanceAdapter(List<AttendanceStatus> attendanceStatusList, Context context) {
        this.attendanceStatusList = attendanceStatusList;
        this.context = context;
    }

    @NonNull
    @Override
    public AttendanceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.attendance_list_row, viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AttendanceAdapter.MyViewHolder myViewHolder, int i) {
        final AttendanceStatus attendanceStatus = attendanceStatusList.get(i);
        myViewHolder.txtName.setText(attendanceStatus.getName());
        myViewHolder.txtId.setText("Admission No: "+attendanceStatus.getId());
        myViewHolder.txtStatus.setText(attendanceStatus.getStatus());
        if (attendanceStatus.getStatus().equals(context.getResources().getString(R.string.present))){
            myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.success));
        }else {
            myViewHolder.txtStatus.setTextColor(context.getResources().getColor(R.color.error));
        }

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return attendanceStatusList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView txtName, txtId, txtStatus;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView)itemView.findViewById(R.id.txtName);
            txtId = (TextView)itemView.findViewById(R.id.txtId);
            txtStatus = (TextView)itemView.findViewById(R.id.txtStatus);
        }
    }
}
