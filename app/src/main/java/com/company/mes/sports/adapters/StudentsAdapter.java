package com.company.mes.sports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.activities.Attendance;
import com.company.mes.sports.activities.HealthHistory;
import com.company.mes.sports.activities.MainActivity;
import com.company.mes.sports.activities.ReportActivity;
import com.company.mes.sports.activities.TrainingActivity;
import com.company.mes.sports.models.Student;

import java.util.List;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.MyViewHolder> {
    private List<Student> studentList;
    private Context context;
    String fragment;

    public StudentsAdapter(List<Student> studentList, Context context, String fragment) {
        this.studentList = studentList;
        this.context = context;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public StudentsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.students_list_row, viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentsAdapter.MyViewHolder myViewHolder, int i) {
        final Student student = studentList.get(i);
        myViewHolder.txtName.setText(student.getId()+" - "+student.getName());
        myViewHolder.txtPhone.setText(student.getPhone());
        myViewHolder.txtAddress.setText(student.getAddress());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = null;
                if (fragment.equals(MainActivity.TAG_STUDENTS))
                    intent = new Intent(context, HealthHistory.class);
                if (fragment.equals(MainActivity.TAG_TRAINING))
                    intent = new Intent(context, TrainingActivity.class);
                if (fragment.equals(MainActivity.TAG_REPORT))
                    intent = new Intent(context, ReportActivity.class);
                intent.putExtra("id", student.getId());
                context.startActivity(intent);
            }
        });
        if (fragment.equals(MainActivity.TAG_STUDENTS)) {
            myViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    new AlertDialog.Builder(context)
                            .setTitle("Student Allocation")
                            .setMessage("Are you sure you want to select this student for training?")
                            .setPositiveButton("Select", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Toast.makeText(context, "Selected", Toast.LENGTH_LONG).show();
                                    student.setSelected(true);
                                    MainActivity.databaseReference.child("students").child(student.getId()).setValue(student);
                                }
                            }).setNegativeButton("Reject", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(context, "Rejected", Toast.LENGTH_LONG).show();
                            student.setSelected(false);
                            MainActivity.databaseReference.child("students").child(student.getId()).setValue(student);
                        }
                    }).show();
                    return false;
                }
            });
        }else {
            if (!student.isSelected()){
                myViewHolder.itemView.setVisibility(View.GONE);
                myViewHolder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            }
        }
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView txtName, txtPhone, txtAddress;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView)itemView.findViewById(R.id.name);
            txtPhone = (TextView)itemView.findViewById(R.id.phone);
            txtAddress = (TextView)itemView.findViewById(R.id.address);
        }
    }
}
