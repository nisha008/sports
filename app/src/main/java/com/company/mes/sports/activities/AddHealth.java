package com.company.mes.sports.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.Health;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AddHealth extends AppCompatActivity {
    EditText edtHeight, edtWeight;
    RadioButton rdoSkinYes, rdoEarYes, rdoEyeYes,rdoBpLow, rdoBpNormal;
    String id;
    String TAG = "AddHealth";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_health);
        getSupportActionBar().setTitle("Add Health Record");

        edtHeight = (EditText)findViewById(R.id.height);
        edtWeight = (EditText)findViewById(R.id.weght);

        rdoSkinYes = (RadioButton)findViewById(R.id.rdo_skin_yes);
        rdoEarYes = (RadioButton)findViewById(R.id.rdo_ear_yes);
        rdoEyeYes = (RadioButton)findViewById(R.id.rdo_eye_yes);
        rdoBpLow = (RadioButton)findViewById(R.id.rdo_bp_low);
        rdoBpNormal = (RadioButton)findViewById(R.id.rdo_bp_normal);

        id = getIntent().getStringExtra("id");
    }

    public void addHealth(View view) {
        if (isValid()){
            Health health = new Health();
            health.setHeight(edtHeight.getText().toString());
            health.setWeight(edtWeight.getText().toString());
            health.setSkin(rdoSkinYes.isChecked() ? "Yes" : "No");
            health.setEar(rdoEarYes.isChecked() ? "Yes" : "No");
            health.setEye(rdoEyeYes.isChecked() ? "Yes" : "No");
            health.setBp(rdoBpLow.isChecked() ? "Low" : (rdoBpNormal.isChecked()? "Normal" : "High"));
            Log.d(TAG, "addHealth: "+health.toString());

            String key = MainActivity.databaseReference.child("students").child(id).child("health").push().getKey();
            health.setId(getCurrentTimeStamp());
            MainActivity.databaseReference.child("students").child(id).child("health").child(health.getId()).setValue(health)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            toast(R.string.successfully_created);
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    toast(R.string.oops);
                }
            });
        }
    }

    private void toast(int msg) {
        Toast.makeText(AddHealth.this, msg, Toast.LENGTH_LONG).show();
    }

    private boolean isValid() {
        return true;
    }

    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
