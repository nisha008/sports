package com.company.mes.sports.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.Health;
import com.company.mes.sports.models.Test;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TestActivity extends AppCompatActivity {

    EditText edtNumber, edtDistance;
    String id;
    String TAG = "TestActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        getSupportActionBar().setTitle("Create Test");

        id = getIntent().getStringExtra("id");
        edtNumber = (EditText)findViewById(R.id.txt_number);
        edtDistance = (EditText)findViewById(R.id.txt_distance);

    }

    public void createTest(View view) {
        if (isValid()){
            Test test = new Test();
            String timestamp = getCurrentTimeStamp();
            test.setId(timestamp);
            test.setNumber(edtNumber.getText().toString());
            test.setDistance(edtDistance.getText().toString());
            MainActivity.databaseReference.child("students").child(id).child("training").child(timestamp).setValue(test)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            toast(R.string.successfully_created);
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    toast(R.string.oops);
                }
            });
        }
    }
    private void toast(int msg) {
        Toast.makeText(TestActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    private boolean isValid() {
        return true;
    }

    public static String getCurrentTimeStamp(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
