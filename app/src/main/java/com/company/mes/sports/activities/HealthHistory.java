package com.company.mes.sports.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.company.mes.sports.R;
import com.company.mes.sports.adapters.HealthAdapter;
import com.company.mes.sports.models.Health;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.List;

public class HealthHistory extends AppCompatActivity {

    private RecyclerView recyclerView;
    private HealthAdapter healthAdapter;
    private List<Health> healthList= new ArrayList<>();
    String TAG = "HealthHistory";
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Health History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id = getIntent().getStringExtra("id");
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(HealthHistory.this, AddHealth.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        healthAdapter = new HealthAdapter(healthList, HealthHistory.this);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(HealthHistory.this);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(healthAdapter);

        ChildEventListener healthEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Health health = dataSnapshot.getValue(Health.class);
                healthList.add(health);
                healthAdapter.notifyDataSetChanged();
                Log.d(TAG, "onChildAdded: " + dataSnapshot.toString());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Health health = dataSnapshot.getValue(Health.class);
                Log.d(TAG, "onChildChanged: " + dataSnapshot.getKey() + health.toString());
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        MainActivity.databaseReference.child("students").child(id).child("health").addChildEventListener(healthEventListener);
    }

}
