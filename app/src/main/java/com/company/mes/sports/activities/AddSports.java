package com.company.mes.sports.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.Sport;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class AddSports extends AppCompatActivity {
    EditText txtItemName;
    RadioButton rdoSingleItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sports);
        getSupportActionBar().setTitle("Add Sport");

        txtItemName = (EditText)findViewById(R.id.item_name);
        rdoSingleItem = (RadioButton)findViewById(R.id.singleItem);
    }

    public void addSports(View view) {
        if (isValid()){
            Sport sport = new Sport()
                    .setName(txtItemName.getText().toString())
                    .setType(rdoSingleItem.isChecked() ? Sport.SINGLE : Sport.GROUP);

            String key = MainActivity.databaseReference.child("sports").push().getKey();
            sport.setId(key);
            MainActivity.databaseReference.child("sports").child(key).setValue(sport)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(AddSports.this, R.string.successfully_created, Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(AddSports.this, R.string.oops, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private boolean isValid() {
        return true;
    }
}
