package com.company.mes.sports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.mes.sports.R;
import com.company.mes.sports.models.AttendanceStatus;
import com.company.mes.sports.models.Test;

import java.util.List;

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.MyViewHolder> {
    private List<Test> testList;
    private Context context;

    public TestAdapter(List<Test> testList, Context context) {
        this.testList = testList;
        this.context = context;
    }

    @NonNull
    @Override
    public TestAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.attendance_list_row, viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TestAdapter.MyViewHolder myViewHolder, int i) {
        final Test test = testList.get(i);
        myViewHolder.txtName.setText("Number: "+test.getNumber());
        myViewHolder.txtId.setText("Timestamp: "+test.getId());
        myViewHolder.txtStatus.setText("Distance: "+test.getDistance());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView txtName, txtId, txtStatus;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView)itemView.findViewById(R.id.txtName);
            txtId = (TextView)itemView.findViewById(R.id.txtId);
            txtStatus = (TextView)itemView.findViewById(R.id.txtStatus);
        }
    }
}
