package com.company.mes.sports.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.Coach;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import static com.company.mes.sports.activities.MainActivity.coach;
import static com.company.mes.sports.activities.MainActivity.database;
import static com.company.mes.sports.activities.MainActivity.databaseReference;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    public static EditText edit_username, edit_password;
    public SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Login");

        edit_username = (EditText)findViewById(R.id.username);
        edit_password = (EditText)findViewById(R.id.password);

        sharedPreferences = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        if (sharedPreferences.contains("username")){
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }

        if (database == null){
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
            databaseReference = database.getReference();
            databaseReference.keepSynced(true);
        }

        ChildEventListener coachEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Coach coach = dataSnapshot.getValue(Coach.class);
                MainActivity.coach = coach;
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        MainActivity.databaseReference.child("coach").addChildEventListener(coachEventListener);
    }
    public void forgot(View view) {
        startActivity(new Intent(this, ResetPasswordActivity.class));
    }

    public void login(View view) {
        if (isVerified()){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("username", coach.getUsername());
            editor.putString("password", coach.getPassword());
            editor.commit();
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
    }

    private boolean isVerified() {
        if (!coach.getUsername().equals(edit_username.getText().toString())){
            Toast.makeText(LoginActivity.this, "Invalid Username", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!coach.getPassword().equals(edit_password.getText().toString())){
            Toast.makeText(LoginActivity.this, "Invalid password", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void signUp(View view) {
        startActivity(new Intent(LoginActivity.this, SignupActivity.class));
        finish();
    }
}
