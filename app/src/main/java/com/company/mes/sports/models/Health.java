package com.company.mes.sports.models;

public class Health {


    int height;// in cm
    double weight;// in gram

    String id, skin, eye, ear, bp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeight() {
        return String.valueOf(height);
    }

    public void setHeight(String height) {
        this.height = Integer.parseInt(height);
    }

    public String getWeight() {
        return String.valueOf(weight);
    }

    public void setWeight(String weight) {
        this.weight = Double.parseDouble(weight);
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getEye() {
        return eye;
    }

    public void setEye(String eye) {
        this.eye = eye;
    }

    public String getEar() {
        return ear;
    }

    public void setEar(String ear) {
        this.ear = ear;
    }

    public String getBp() {
        return bp;
    }

    public void setBp(String bp) {
        this.bp = bp;
    }

    public double getBMI(){
        return (this.weight/1000)/((this.height/100)^2);
    }
}
