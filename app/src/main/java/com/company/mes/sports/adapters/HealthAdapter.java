package com.company.mes.sports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.mes.sports.R;
import com.company.mes.sports.activities.Attendance;
import com.company.mes.sports.activities.HealthHistory;
import com.company.mes.sports.models.Health;

import java.util.List;

public class HealthAdapter extends RecyclerView.Adapter<HealthAdapter.MyViewHolder> {
    private List<Health> healthList;
    private Context context;

    public HealthAdapter(List<Health> healthList, Context context) {
        this.healthList = healthList;
        this.context = context;
    }

    @NonNull
    @Override
    public HealthAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.health_list_row, viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HealthAdapter.MyViewHolder myViewHolder, int i) {
        final Health health = healthList.get(i);
        myViewHolder.txtTitle.setText(health.getId());
        myViewHolder.txtHeight.setText("Height: "+health.getHeight()+"cm");
        myViewHolder.txtWeight.setText("Weight: "+health.getWeight()+"gm");
        myViewHolder.txtBMI.setText("BMI: "+health.getBMI());
        myViewHolder.txtSkin.setText("Skin: "+health.getSkin());
        myViewHolder.txtEye.setText("Eye: "+health.getEye());
        myViewHolder.txtEar.setText("Ear: "+health.getEar());
        myViewHolder.txtBp.setText("Bp: "+health.getBp());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HealthHistory.class);
                intent.putExtra("id", health.getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return healthList.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView txtTitle, txtHeight, txtWeight, txtBMI, txtSkin, txtEye, txtEar, txtBp;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView)itemView.findViewById(R.id.txtTitle);
            txtHeight = (TextView)itemView.findViewById(R.id.txtHeight);
            txtWeight = (TextView)itemView.findViewById(R.id.txtWeight);
            txtBMI = (TextView)itemView.findViewById(R.id.txtBMI);
            txtSkin = (TextView)itemView.findViewById(R.id.txtSkin);
            txtEye = (TextView)itemView.findViewById(R.id.txtEye);
            txtEar = (TextView)itemView.findViewById(R.id.txtEar);
            txtBp = (TextView)itemView.findViewById(R.id.txtBp);
        }
    }
}
