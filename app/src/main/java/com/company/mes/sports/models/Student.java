package com.company.mes.sports.models;


public class Student {


    private String id, name, dob, gender, guardian, address, phone, blood;
    private Sport interested_singe_item, interested_group_item;
    private String standard;

    boolean selected = false;

    public Student() {

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public Student setId(String id) {
        this.id = id;
        return Student.this;
    }

    public String getName() {
        return name;
    }

    public Student setName(String name) {
        this.name = name;
        return Student.this;
    }

    public String getDob() {
        return dob;
    }

    public Student setDob(String dob) {
        this.dob = dob;
        return Student.this;
    }

    public String getGender() {
        return gender;
    }

    public Student setGender(String gender) {
        this.gender = gender;
        return Student.this;
    }

    public String getStandard() {
        return standard;
    }

    public Student setStandard(String standard) {
        this.standard = standard;
        return Student.this;
    }

    public String getGuardian() {
        return guardian;
    }

    public Student setGuardian(String guardian) {
        this.guardian = guardian;
        return Student.this;
    }

    public String getAddress() {
        return address;
    }

    public Student setAddress(String address) {
        this.address = address;
        return Student.this;
    }

    public String getPhone() {
        return phone;
    }

    public Student setPhone(String phone) {
        this.phone = phone;
        return Student.this;
    }

    public String getBlood() {
        return blood;
    }

    public Student setBlood(String blood) {
        this.blood = blood;
        return Student.this;
    }

    public Sport getInterested_singe_item() {
        return interested_singe_item;
    }

    public Student setInterested_singe_item(Sport interested_singe_item) {
        this.interested_singe_item = interested_singe_item;
        return Student.this;
    }

    public Sport getInterested_group_item() {
        return interested_group_item;
    }

    public Student setInterested_group_item(Sport interested_group_item) {
        this.interested_group_item = interested_group_item;
        return Student.this;
    }

}
