package com.company.mes.sports.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.Coach;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;

import static com.company.mes.sports.activities.MainActivity.database;
import static com.company.mes.sports.activities.MainActivity.databaseReference;

public class SignupActivity extends AppCompatActivity {

    EditText username, password, confirm_password, mobile, email;
    public SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getSupportActionBar().setTitle("Sign Up");
        sharedPreferences = getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        confirm_password = (EditText)findViewById(R.id.confirm_password);
        mobile = (EditText)findViewById(R.id.mobile);
        email = (EditText)findViewById(R.id.email);

        if (database == null){
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
            databaseReference = database.getReference();
            databaseReference.keepSynced(true);
        }
    }
    public void signIn(View view) {
        startActivity(new Intent(SignupActivity.this, LoginActivity.class));
        finish();
    }

    public void signUp(View view) {
        if (isValid()){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("username", username.getText().toString());
            editor.putString("password", password.getText().toString());
            editor.commit();
            Coach coach = new Coach();
            coach.setName(username.getText().toString());
            coach.setUsername(username.getText().toString());
            coach.setEmail(email.getText().toString());
            coach.setMobile(mobile.getText().toString());
            coach.setPassword(password.getText().toString());
            coach.setPhoto("link here");

            databaseReference.child("coach").child("profile").setValue(coach)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(SignupActivity.this, "Account created successfully", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(SignupActivity.this, R.string.oops, Toast.LENGTH_LONG).show();
                }
            });

        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(username.getText())){
            Toast.makeText(SignupActivity.this, "Username required", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(password.getText())){
            Toast.makeText(SignupActivity.this, "Password required", Toast.LENGTH_LONG).show();
            return false;
        }
        if (!password.getText().toString().equals(confirm_password.getText().toString())){
            Toast.makeText(SignupActivity.this, "Password doesn't match!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(mobile.getText())){
            Toast.makeText(SignupActivity.this, "Mobile number required", Toast.LENGTH_LONG).show();
            return false;
        }
        if (TextUtils.isEmpty(email.getText())){
            Toast.makeText(SignupActivity.this, "Email required", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
