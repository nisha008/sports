package com.company.mes.sports.models;

public class ClassGroup {
    String id, fromClass, toClass, bmi;

    public String getId() {
        return id;
    }

    public ClassGroup setId(String id) {
        this.id = id;
        return ClassGroup.this;
    }

    public String getFromClass() {
        return fromClass;
    }

    public ClassGroup setFromClass(String fromClass) {
        this.fromClass = fromClass;
        return ClassGroup.this;
    }

    public String getToClass() {
        return toClass;
    }

    public ClassGroup setToClass(String toClass) {
        this.toClass = toClass;
        return ClassGroup.this;
    }

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getName() {
        String name = this.getFromClass() + " - " + this.getToClass();
        if (bmi != null)
            name += "   bmi("+this.getBmi()+")";
        return name;
    }
}
