package com.company.mes.sports.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.ClassGroup;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import static com.company.mes.sports.activities.MainActivity.databaseReference;
import static com.company.mes.sports.activities.MainActivity.standardsList;
import static com.company.mes.sports.activities.MainActivity.standards_adapter;

public class SetupClass extends AppCompatActivity {
    Spinner sprFromClass, sprToClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_class);
        getSupportActionBar().setTitle("Setup Class Group");

        sprFromClass = (Spinner)findViewById(R.id.spr_from_class);
        sprToClass = (Spinner)findViewById(R.id.spr_to_class);

        sprFromClass.setAdapter(standards_adapter);
        sprToClass.setAdapter(standards_adapter);
    }

    public void createClassGroup(View view) {
        String key = databaseReference.child("class_group").push().getKey();
        ClassGroup classGroup = new ClassGroup();
        classGroup.setId(key)
                .setFromClass(standardsList.get(sprFromClass.getSelectedItemPosition()))
                .setToClass(standardsList.get(sprToClass.getSelectedItemPosition()));
        databaseReference.child("class_group").child(key).setValue(classGroup)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        toast(R.string.successfully_created);
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                toast(R.string.oops);
            }
        });
    }

    private void toast(int msg) {
        Toast.makeText(SetupClass.this, msg, Toast.LENGTH_LONG).show();
    }
}
