package com.company.mes.sports.models;

public class Sport {
    public static final int ALL = 0;
    public static final int SINGLE = 1;
    public static final int GROUP = 2;
    String id, name;
    int type;

    public String getName() {
        return name;
    }

    public Sport setName(String name) {
        this.name = name;
        return Sport.this;
    }

    public int getType() {
        return type;
    }

    public Sport setType(int type) {
        this.type = type;
        return Sport.this;
    }

    public String getId() {
        return id;
    }

    public Sport setId(String id) {
        this.id = id;
        return Sport.this;
    }

    @Override
    public String toString() {
        return "Sport{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
