package com.company.mes.sports.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.company.mes.sports.R;
import com.company.mes.sports.fragments.AttendanceFragment;
import com.company.mes.sports.fragments.ReportFragment;
import com.company.mes.sports.fragments.ScheduleFragment;
import com.company.mes.sports.fragments.SettingsFragment;
import com.company.mes.sports.fragments.StudentsFragment;
import com.company.mes.sports.fragments.TrainingFragment;
import com.company.mes.sports.models.ClassGroup;
import com.company.mes.sports.models.Coach;
import com.company.mes.sports.models.Sport;
import com.company.mes.sports.models.Student;
import com.company.mes.sports.others.DatePickerFragment;
import com.company.mes.sports.others.TimePickerFragment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        StudentsFragment.OnFragmentInteractionListener,
        AttendanceFragment.OnFragmentInteractionListener,
        ScheduleFragment.OnFragmentInteractionListener,
        TrainingFragment.OnFragmentInteractionListener,
        ReportFragment.OnFragmentInteractionListener,
        SettingsFragment.OnFragmentInteractionListener,
        DatePickerFragment.OnDateSetListener,
        TimePickerFragment.OnTimeSetListener {
    String TAG = "MAIN_ACTIVITY";

    public static List<Student> studentList= new ArrayList<>();
    public static ArrayList<Sport> sportsList = new ArrayList<>();
    public static List<ClassGroup> classGroupList = new ArrayList<>();
    public static Coach coach = null;
    // urls to load navigation header background image
    // and profile image
    private static String urlNavHeaderBg = "https://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    public static final String TAG_STUDENTS = "Students";
    public static final String TAG_SHEDULE = "Schedule";
    public static final String TAG_ATTENDANCE = "Attendance";
    public static final String TAG_TRAINING = "Training";
    public static final String TAG_REPORT = "Report";
    public static final String TAG_SETTINGS = "Settings";
    public static String CURRENT_TAG = TAG_STUDENTS;
    public static List<String> standardsList = new ArrayList<>();
    public static ArrayAdapter<String> standards_adapter;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    TextView txtName, txtEmail;
    ImageView imgProfile, imgNavHeaderBg;
    DrawerLayout drawer;
    View navHeader;
    NavigationView navigationView;
    FloatingActionButton fab;

    private ChildEventListener userEventListener;
    public static FirebaseDatabase database = null;
    public static DatabaseReference databaseReference;
    public static Calendar calendar = Calendar.getInstance();
    public static ArrayAdapter<String> class_groups_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mHandler = new Handler();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CURRENT_TAG == TAG_STUDENTS)
                    startActivity(new Intent(MainActivity.this, AddStudent.class));
                if (CURRENT_TAG == TAG_SHEDULE)
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                if (CURRENT_TAG == TAG_ATTENDANCE)
                    startActivity(new Intent(MainActivity.this, SignupActivity.class));

            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView)navHeader.findViewById(R.id.name);
        txtEmail = (TextView)navHeader. findViewById(R.id.email);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
        // load nav menu header data
        loadNavHeader();


        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_STUDENTS;
            loadHomeFragment();
        }
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Write a message to the database
        if (database == null){
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
            databaseReference = database.getReference();
            databaseReference.keepSynced(true);
        }

        studentList.clear();
        standardsList.add("1");
        standardsList.add("2");
        standardsList.add("3");
        standardsList.add("4");
        standardsList.add("5");
        standardsList.add("6");
        standardsList.add("7");
        standardsList.add("8");
        standardsList.add("9");
        standardsList.add("10");
        standardsList.add("+1");
        standardsList.add("+2");
        standards_adapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                standardsList);
        class_groups_adapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                classGroupNameList(classGroupList));


        studentList.clear();
        ChildEventListener userEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Student student = dataSnapshot.getValue(Student.class);
                MainActivity.studentList.add(student);
                if (CURRENT_TAG.equals(TAG_STUDENTS)){
                    StudentsFragment studentsFragment = (StudentsFragment)getSupportFragmentManager().findFragmentByTag(TAG_STUDENTS);
                    if (studentsFragment != null)
                            studentsFragment.notifyDataSetChanged();
                }
                Log.d(TAG, "onChildAdded: " + dataSnapshot.toString());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Student student = dataSnapshot.getValue(Student.class);
                Log.d(TAG, "onChildChanged: " + dataSnapshot.getKey() + student.toString());
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        MainActivity.databaseReference.child("students").addChildEventListener(userEventListener);

        sportsList.clear();
        ChildEventListener sportsEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Sport sport = dataSnapshot.getValue(Sport.class);
                sportsList.add(sport);
                if (CURRENT_TAG.equals(TAG_SHEDULE)){
                    ScheduleFragment scheduleFragment = (ScheduleFragment) getSupportFragmentManager().findFragmentByTag(TAG_STUDENTS);
                    if (scheduleFragment != null)
                        scheduleFragment.notifyDataSetChanged();
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        MainActivity.databaseReference.child("sports").addChildEventListener(sportsEventListener);

        classGroupList.clear();
        ChildEventListener classGroupEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                ClassGroup classGroup = dataSnapshot.getValue(ClassGroup.class);
                classGroupList.add(classGroup);
                class_groups_adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        MainActivity.databaseReference.child("class_group").addChildEventListener(classGroupEventListener);

        ChildEventListener coachEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Coach coach = dataSnapshot.getValue(Coach.class);
                MainActivity.coach = coach;
                urlProfileImg = coach.getPhoto();
                loadNavHeader();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Coach coach = dataSnapshot.getValue(Coach.class);
                MainActivity.coach = coach;
                urlProfileImg = coach.getPhoto();
                loadNavHeader();
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        databaseReference.child("coach").addChildEventListener(coachEventListener);
        
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // Handle navigation view menuItem clicks here.
        int id = menuItem.getItemId();

        if (id == R.id.nav_students) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_STUDENTS;
        } else if (id == R.id.nav_schedule) {
            navItemIndex = 1;
            CURRENT_TAG = TAG_SHEDULE;
        } else if (id == R.id.nav_attendance) {
            navItemIndex = 2;
            CURRENT_TAG = TAG_ATTENDANCE;
        } else if (id == R.id.nav_training) {
            navItemIndex = 3;
            CURRENT_TAG = TAG_TRAINING;
        } else if (id == R.id.nav_report) {
            navItemIndex = 4;
            CURRENT_TAG = TAG_REPORT;
        } else if (id == R.id.nav_settings) {
            navItemIndex = 5;
            CURRENT_TAG = TAG_SETTINGS;
        } else if (id == R.id.nav_sign_out) {
            signOut();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        //Checking if the item is in checked state or not, if not make it in checked state
        if (menuItem.isChecked()) {
            menuItem.setChecked(false);
        } else {
            menuItem.setChecked(true);
        }
        menuItem.setChecked(true);

        loadHomeFragment();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void signOut() {
        SharedPreferences sharedPreferences = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    private void loadNavHeader() {
        // name, website
        txtName.setText(coach == null ? "Ravi Tamada" : coach.getName());
        txtEmail.setText(coach == null ? "www.androidhive.info" : coach.getEmail());

        // loading header background image
        Glide.with(getApplicationContext()).load(urlNavHeaderBg)
                .transition(new DrawableTransitionOptions().crossFade())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);

        // Loading profile image
        Glide.with(getApplicationContext()).load(urlProfileImg)
                .transition(new DrawableTransitionOptions().crossFade())
                .thumbnail(0.5f)
                .apply(RequestOptions.circleCropTransform())
                .into(imgProfile);

    }
    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {


        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }
    // show or hide the fab
    private void toggleFab() {
        if (CURRENT_TAG == TAG_STUDENTS || CURRENT_TAG == TAG_ATTENDANCE || CURRENT_TAG == TAG_SHEDULE)
            fab.show();
        else
            fab.hide();
    }


    @NonNull
    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                return new StudentsFragment();
            case 1:
                return new ScheduleFragment();
            case 2:
                return new AttendanceFragment();
            case 3:
                return new TrainingFragment();
            case 4:
                return new ReportFragment();
            case 5:
                return new SettingsFragment();
            default:
                return new StudentsFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        if (CURRENT_TAG.equals(TAG_SHEDULE))
            ((ScheduleFragment)getSupportFragmentManager().findFragmentByTag(TAG_SHEDULE))
                .onDateSet(view, year, month, day);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        if (CURRENT_TAG.equals(TAG_SHEDULE))
            ((ScheduleFragment)getSupportFragmentManager().findFragmentByTag(TAG_SHEDULE))
                    .onTimeSet(view,hourOfDay,minute);
    }

    public static List<Sport> sportsList(int filter) {
        List<Sport> sportsList = new ArrayList<>();
        for (Sport sport : MainActivity.sportsList) {
            if (filter != Sport.ALL && sport.getType()!=filter)
                continue;
            sportsList.add(sport);
        }
        return sportsList;
    }
    public static List<String> sportsNameList(List<Sport> sports){
        List<String> sportsNameList = new ArrayList<>();
        for (Sport sport : sports) {
            sportsNameList.add(sport.getName());
        }
        return sportsNameList;
    }

    public static List<String> classGroupNameList(List<ClassGroup> classGroupList){
        List<String> classGroupNameList = new ArrayList<>();
        for (ClassGroup classGroup : classGroupList) {
            classGroupNameList.add(classGroup.getName());
        }
        return classGroupNameList;
    }
}
