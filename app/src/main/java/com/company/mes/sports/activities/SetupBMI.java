package com.company.mes.sports.activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.company.mes.sports.R;
import com.company.mes.sports.models.ClassGroup;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import static com.company.mes.sports.activities.MainActivity.classGroupList;
import static com.company.mes.sports.activities.MainActivity.classGroupNameList;
import static com.company.mes.sports.activities.MainActivity.class_groups_adapter;

public class SetupBMI extends AppCompatActivity {

    Spinner sprClassGroup;
    EditText edtBMI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_bmi);
        getSupportActionBar().setTitle("Setup BMI");

        sprClassGroup = (Spinner)findViewById(R.id.spr_class_group);
        class_groups_adapter = new ArrayAdapter<>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                classGroupNameList(classGroupList));
        sprClassGroup.setAdapter(class_groups_adapter);

        edtBMI = (EditText)findViewById(R.id.edt_bmi);
    }

    public void setBMI(View view) {
        if (!isValid())return;
        ClassGroup classGroup = classGroupList.get(sprClassGroup.getSelectedItemPosition());
        classGroup.setBmi(edtBMI.getText().toString());
        MainActivity.databaseReference.child("class_group").child(classGroup.getId()).setValue(classGroup)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        toast(R.string.successfully_updated);
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                toast(R.string.oops);
            }
        });
    }

    private void toast(int msg) {
        Toast.makeText(SetupBMI.this, msg, Toast.LENGTH_LONG).show();
    }

    private boolean isValid() {
        return true;
    }
}
